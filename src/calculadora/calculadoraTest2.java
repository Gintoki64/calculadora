package calculadora;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class calculadoraTest2 {

	

	@Test
	void testSumar() {
		calculadora p = new calculadora();
		assertEquals(4,p.sumar(2, 2),0);
		assertEquals(6,p.sumar(4, 2),0);
		assertEquals(3,p.sumar(1, 2),0);
	}

	@Test
	void testRestar() {
		calculadora p = new calculadora();
		assertEquals(0,p.restar(2, 2),0);
	}

	@Test
	void testMultiplicar() {
		calculadora p = new calculadora();
		assertEquals(4,p.multiplicar(2, 2),0);
	}

}
